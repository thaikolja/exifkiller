#!python3

# Import required modules
import sys
import os
import pip
import argparse


# Deletes all EXIF data from a given image file
def delete_exif():
    # Set up the argument parser
    parser = argparse.ArgumentParser(
        description='Removes all EXIF data from an image file'
    )

    # Register first argument which will be the absolute image path
    parser.add_argument(
        type=str,
        help='Absolute path to image',
        dest='image_path',
        action='store'
    )

    # Catch the argument
    arguments = parser.parse_args()

    # Converting the image path into a string variable
    image_path = str(arguments.image_path)

    # Check if the target file exists
    if not os.path.exists(image_path):
        # Display an error message and stop executing the script if file cannot be found
        sys.exit(f'ERROR: The image file {image_path} could not be found.')

    # Delete the EXIF data from the image
    exif_delete.batch_exif_delete(
        [image_path],
        replace=True
    )

    # Display an output message
    sys.exit('\nDONE: All EXIF data has been removed.')


# Check if the required `exif_delete` module exists
try:
    import exif_delete

    # If yes, delete the EXIF data
    delete_exif()

except ModuleNotFoundError:
    # If not, attempt to install it
    pip.main(['install', 'exif_delete'])

    # Import the module
    import exif_delete

    # Delete the EXIF data if the module is loaded
    delete_exif()
