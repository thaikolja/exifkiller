# EXIF Killer

This small script - programmed in Python 3 - lets you delete all [EXIF data](https://wethegeek.com/exif-dangers-you-should-be-aware-of-and-how-to-avoid-them/) from any image file via a simple, one-line command.

## General

### 1. Usage

1. [Download](https://gitlab.com/thaikolja/exifkiller/-/archive/master/exifkiller-master.zip) or clone this repository
2. Use `cd` to go into the directory wherever you cloned or extracted the repository to
3. Run the either the executable or the Python script via a terminal by running any of the following commands:
   1. `./exifkiller test-image.jpg` (as executable)
   2. `./exifkiller.py test-image.jpg` (as Python 3 script)

**Please note that the original file will be overwritten.**

Feel free to test the script with the following image:

![Test image with EXIF data](https://gitlab.com/thaikolja/exifkiller/-/raw/master/test-image.jpg)

### 2. Requirements

To use this script, you will need a terminal such as Terminal or [iTerm2](https://iterm2.com/downloads.html) (macOS), or a Windows command line interface (e.g. the native [Command Prompt](https://www.howtogeek.com/235101/10-ways-to-open-the-command-prompt-in-windows-10/#opencommandpromptadminfromtaskmanager), [Windows Terminal](https://www.microsoft.com/en-us/p/windows-terminal/9n0dx20hk701?activetab=pivot:overviewtab), or [PowerShell](https://github.com/PowerShell/PowerShell/releases/latest).

Furthermore, you need to have **Python 3** installed. Learn how to [install Python 3 under macOS](https://installpython3.com/mac/) and [under Windows](https://realpython.com/installing-python/#how-to-install-from-the-microsoft-store).

*EXIF Killer* requires the Python 3 module `exif_delete`. Upon running the script, it should install it automatically. However, if you wish to install it directly, use `pip3 install exif_delete`.

## Collaboration

Despite this being a tiny script, anyone is invited to collaborate in making it better. For that, please [fork this repository](https://gitlab.com/thaikolja/exifkiller/-/forks/new), add your improvements, and [create a merge request](https://gitlab.com/thaikolja/exifkiller/-/merge_requests) here on GitLab.

Feel free to contact me at [kolja.nolte@gmail.com](mailto:kolja.nolte@gmail.com).